<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsTable extends Migration {

    public function up(){
        Schema::create("lots", function (Blueprint $table) {

            $table->integer('id');			$table->integer('car_id');			$table->integer('round_id');			$table->string('created_at');			$table->integer('auction_type');			$table->integer('status');			$table->integer('profile_id');			$table->string('price_step');

        });
    }

    public function down(){
        Schema::dropIfExists("lots");
    }

}

