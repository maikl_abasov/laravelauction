<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up(){
        Schema::create("users", function (Blueprint $table) {

            $table->integer('id');			$table->string('email');			$table->string('password');			$table->string('username');			$table->string('phone');			$table->integer('role');			$table->string('created_at');			$table->string('updated_at');			$table->integer('active');			$table->string('verify');

        });
    }

    public function down(){
        Schema::dropIfExists("users");
    }

}

