<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvcMarksTable extends Migration {

    public function up(){
        Schema::create("avc_marks", function (Blueprint $table) {

            $table->integer('id');			$table->string('name');			$table->string('name_rus');			$table->string('id_car_mark');

        });
    }

    public function down(){
        Schema::dropIfExists("avc_marks");
    }

}

