<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCbnLotsTable extends Migration {

    public function up(){
        Schema::create("cbn_lots", function (Blueprint $table) {

            $table->integer('id');			$table->integer('acc_id');			$table->integer('num');			$table->integer('category');			$table->string('date_start');			$table->string('date_end');			$table->integer('city');			$table->string('l_type');			$table->string('ascdesc');			$table->integer('start_price');			$table->integer('price_step');			$table->integer('price_now');			$table->integer('price_min');			$table->string('nds');			$table->integer('mark');			$table->integer('model');			$table->integer('generation');			$table->integer('modification');			$table->text('self_modification');			$table->string('body');			$table->string('vin');			$table->integer('year');			$table->integer('mileage');			$table->string('engine');			$table->string('engine_vol');			$table->integer('power');			$table->string('gearbox');			$table->string('color');			$table->string('key_qty');			$table->string('reg_plate');			$table->string('drive');			$table->text('description');			$table->string('pts');			$table->string('pts_no');			$table->string('sts_no');			$table->string('serv_book');			$table->text('comments');			$table->text('view_place');			$table->string('dkp');			$table->string('status');			$table->text('admin_comment');			$table->text('view_descr');			$table->integer('pay_block');			$table->integer('comission');			$table->string('owners_count');			$table->string('auto_complectation');			$table->integer('round_id');			$table->string('classified_status');			$table->integer('freedelivery_status');			$table->string('video');			$table->string('car_class_code');			$table->string('created_dt');			$table->string('fid_load_type');

        });
    }

    public function down(){
        Schema::dropIfExists("cbn_lots");
    }

}

