<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration {

    public function up(){
        Schema::create("bids", function (Blueprint $table) {

            $table->integer('id');			$table->integer('lot_id');			$table->integer('profile_id');			$table->string('price');			$table->string('created_at');			$table->integer('round_id');			$table->integer('confirm_state');			$table->string('confirm_date');

        });
    }

    public function down(){
        Schema::dropIfExists("bids");
    }

}

