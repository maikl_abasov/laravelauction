<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsAuctionTable extends Migration {

    public function up(){
        Schema::create("rounds_auction", function (Blueprint $table) {

            $table->integer('id');			$table->string('start_date');			$table->string('end_date');			$table->integer('auction_type');			$table->string('created_at');			$table->string('date');			$table->string('time');

        });
    }

    public function down(){
        Schema::dropIfExists("rounds_auction");
    }

}

