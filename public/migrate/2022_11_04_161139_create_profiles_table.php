<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

    public function up(){
        Schema::create("profiles", function (Blueprint $table) {

            $table->integer('id');			$table->string('name');			$table->string('email');			$table->string('phone');			$table->integer('type');			$table->integer('user_id');			$table->string('created_at');			$table->string('updated_at');			$table->integer('active');

        });
    }

    public function down(){
        Schema::dropIfExists("profiles");
    }

}

