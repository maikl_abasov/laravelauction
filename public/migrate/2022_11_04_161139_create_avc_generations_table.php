<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvcGenerationsTable extends Migration {

    public function up(){
        Schema::create("avc_generations", function (Blueprint $table) {

            $table->integer('id');			$table->integer('model');			$table->string('name');			$table->string('id_car_generation');

        });
    }

    public function down(){
        Schema::dropIfExists("avc_generations");
    }

}

