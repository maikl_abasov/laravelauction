<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoundAuctionTest extends TestCase
{

    protected $startDate = '2022-09-19 10:37';
    protected $auctionType = 1;
    protected $postData = [
        'auction_type' => 1,
        'start_date'   => '',
        'end_date'     => '2022-09-19 18:20',
        'date'         => '2022-09-19',
        'time'         => '10:35',
        'profile_id'   => 1,
    ];

    protected function getValidatorErros($response) {
        $messageData = $response->exception->errors();
        return $messageData;
    }

    public function testCreateRoundValidateError()
    {
        $auctionType = $this->auctionType;
        $startDate = $this->startDate;
        $data = $this->postData;
        $data['auction_type'] = $auctionType;

        $response = $this->post('/api/admin/round-auction/create', $data);
        $messageData  = $this->getValidatorErros($response);
        $errorMessage = $messageData['start_date'][0];
        $response->assertStatus(302);
        $this->assertSame('Начало аукциона не заполнено', $errorMessage);
        // dd($errorMessage);

    }

    public function testCreateRound()
    {
        $auctionType = $this->auctionType;
        $startDate = $this->startDate;

        $data = $this->postData;
        $data['auction_type'] = $auctionType;
        $data['start_date'] = $startDate;

        $response = $this->post('/api/admin/round-auction/create', $data);
        $response->assertStatus(200);
        $json = $response->json();
        $this->assertSame(true, $json['result']['save']);
        // dd($jsonResponse);
    }

    public function testGetAllRondsDeleteRound()
    {
        $auctionType = $this->auctionType;
        $startDate = $this->startDate;

        $round   = [];
        $roundId = 0;

        $response = $this->get('/api/admin/round-auction/all-rounds/' .$auctionType. '/0');
        $response->assertStatus(200);
        $json    = $response->json();
        foreach ($json['result'] as $key => $item) {
            $start = $item['start_date'];
            if($startDate == $start) {
                $roundId = $item['id'];
                $round = $item;
                break;
            }
        }

        $response = $this->get('/api/admin/round-auction/delete/' . $roundId);
        $response->assertStatus(200);
        // dd($round);
    }
}
