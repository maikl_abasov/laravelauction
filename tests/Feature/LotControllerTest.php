<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LotControllerTest extends TestCase
{
    protected $status = 1;
    protected $auctionType = 1;
    protected $carVins = "XWB4A11CDBA555020176
                          Z8TNC5FS0BM5008761457";

    protected $postData = [
         'car_vins'     => '',
         'round_id'     => 12,
         'auction_type' =>  1,
         'profile_id'  => 1,
         'category_id' => 1,
         'status'      => 1,
    ];

    protected function getValidatorErros($response) {
        $messageData = $response->exception->errors();
        return $messageData;
    }

    public function testCreateLotsValidateError()
    {
        $auctionType = $this->auctionType;
        $status      = $this->status;
        $data = $this->postData;
        $data['auction_type'] = $auctionType;
        $data['status'] = $status;

        $url = '/api/admin/lot-auction/add-lots';
        $response = $this->post($url, $data);
        $messageData  = $this->getValidatorErros($response);
        $errorMessage = $messageData['car_vins'][0];
        $response->assertStatus(302);
        $this->assertSame('Нет данных авто (vin)', $errorMessage);
        // dd($errorMessage);

    }
//
//    public function testCreateRound()
//    {
//        $auctionType = $this->auctionType;
//        $startDate = $this->startDate;
//
//        $data = $this->postData;
//        $data['auction_type'] = $auctionType;
//        $data['start_date'] = $startDate;
//
//        $response = $this->post('/api/admin/round-auction/create', $data);
//        $response->assertStatus(200);
//        $json = $response->json();
//        $this->assertSame(true, $json['result']['save']);
//        // dd($jsonResponse);
//    }
//
//    public function testGetAllRondsDeleteRound()
//    {
//        $auctionType = $this->auctionType;
//        $startDate = $this->startDate;
//
//        $round = [];
//        $roundId = 0;
//
//        $response = $this->get('/api/admin/round-auction/all-rounds/' . $auctionType . '/0');
//        $response->assertStatus(200);
//        $json = $response->json();
//        foreach ($json['result'] as $key => $item) {
//            $start = $item['start_date'];
//            if ($startDate == $start) {
//                $roundId = $item['id'];
//                $round = $item;
//                break;
//            }
//        }
//
//        $response = $this->get('/api/admin/round-auction/delete/' . $roundId);
//        $response->assertStatus(200);
//        // dd($round);
//    }
}
