<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvcModificationsTable extends Migration {

    public function up(){
        Schema::create("avc_modifications", function (Blueprint $table) {

            $table->integer('id');			$table->integer('generation');			$table->string('name');			$table->integer('year_from');			$table->integer('year_to');			$table->string('body_type');			$table->string('doors');			$table->string('fuel_type');			$table->string('drive_type');			$table->string('transmission');			$table->integer('power');			$table->string('engine_size');			$table->string('options_id');			$table->string('id_car_modification');

        });
    }

    public function down(){
        Schema::dropIfExists("avc_modifications");
    }

}

