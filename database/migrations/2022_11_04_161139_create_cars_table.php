<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration {

    public function up(){
        Schema::create("cars", function (Blueprint $table) {

            $table->id();
			$table->string('vin');
			$table->string('run');
			$table->string('created_at');
			$table->string('updated_at');
			$table->string('year');
			$table->string('pts');
			$table->string('comment');
			$table->integer('mark_id');
			$table->integer('model_id');
			$table->integer('modification_id');
			$table->integer('generation_id');
			$table->integer('start_price');
			$table->integer('max_price');
			$table->string('nds');
			$table->integer('city');
			$table->string('place');
			$table->integer('profile_id');
			$table->string('color');
			$table->string('video');

        });
    }

    public function down(){
        Schema::dropIfExists("cars");
    }

}

