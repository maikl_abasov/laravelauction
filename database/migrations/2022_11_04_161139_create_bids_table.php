<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration {

    public function up(){
        Schema::create("bids", function (Blueprint $table) {

            $table->id();
			$table->integer('lot_id');
            $table->string('price');
			$table->integer('profile_id');
			$table->integer('round_id')->nullable();
			$table->integer('confirm_status')->nullable();
			$table->string('confirm_date')->nullable();
            $table->integer('bid_status')->nullable();
            $table->timestamps();

        });
    }

    public function down(){
        Schema::dropIfExists("bids");
    }

}

