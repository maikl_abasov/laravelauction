<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

    public function up(){
        Schema::create("profiles", function (Blueprint $table) {

            $table->id();
			$table->string('name');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->integer('profile_type');
			$table->integer('user_id');
			$table->integer('active')->nullable();
            $table->timestamps();

        });
    }

    public function down(){
        Schema::dropIfExists("profiles");
    }

}

