<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPhotosTable extends Migration {

    public function up(){
        Schema::create("car_photos", function (Blueprint $table) {

            $table->id();
			$table->integer('car_id');
			$table->string('photo');
			$table->string('created_at');

        });
    }

    public function down(){
        Schema::dropIfExists("car_photos");
    }

}

