<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up(){
        Schema::create("users", function (Blueprint $table) {

            $table->id();
            $table->string('username')->nullable();
			$table->string('email')->unique();
			$table->string('password');

			$table->string('phone')->nullable();
			$table->integer('role')->nullable();
			$table->integer('active')->nullable();
			$table->string('verify')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();


        });
    }

    public function down(){
        Schema::dropIfExists("users");
    }

}

