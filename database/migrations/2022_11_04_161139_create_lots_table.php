<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsTable extends Migration {

    public function up(){
        Schema::create("lots", function (Blueprint $table) {

            $table->id();
            $table->integer('round_id');
            $table->integer('category_id')->default(1);
            $table->integer('status')->default(1);
            $table->integer('profile_id')->nullable();
			$table->integer('car_id')->nullable();
			$table->integer('auction_type')->default(1);
            $table->timestamps();
            $table->string('price_step')->nullable();

        });
    }

    public function down(){
        Schema::dropIfExists("lots");
    }

}

