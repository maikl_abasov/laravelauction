<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCitiesTable extends Migration {

    public function up(){
        Schema::create("geo_cities", function (Blueprint $table) {

            $table->integer('city_id');			$table->string('city');			$table->string('region');			$table->string('district');			$table->string('lat');			$table->string('lng');			$table->integer('sort_city');			$table->integer('sort_region');			$table->integer('delivery');

        });
    }

    public function down(){
        Schema::dropIfExists("geo_cities");
    }

}

