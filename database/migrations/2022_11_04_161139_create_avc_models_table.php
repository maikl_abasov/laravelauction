<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvcModelsTable extends Migration {

    public function up(){
        Schema::create("avc_models", function (Blueprint $table) {

            $table->integer('id');			$table->integer('mark');			$table->string('name');			$table->string('name_rus');			$table->string('id_car_model');

        });
    }

    public function down(){
        Schema::dropIfExists("avc_models");
    }

}

