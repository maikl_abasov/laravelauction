<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsAuctionTable extends Migration {

    public function up(){
        Schema::create("rounds_auction", function (Blueprint $table) {

            $table->id();
			$table->string('start_date');
			$table->string('end_date');
			$table->integer('auction_type');
			$table->string('date')->nullable();
			$table->string('time')->nullable();
            $table->string('s_date')->nullable();
            $table->string('s_time')->nullable();
            $table->integer('profile_id')->nullable();

            $table->timestamps();

        });
    }

    public function down(){
        Schema::dropIfExists("rounds_auction");
    }

}

