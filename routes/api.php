<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RoundAuctionController;
use App\Http\Controllers\LotController;
use App\Http\Controllers\AuctionController;
use App\Http\Controllers\BidController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SecurityController;


use App\Http\Controllers\MakeMigrateWorkerController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//////////////////////////////////////////
// ------- ОСНОВНАЯ ЧАСТЬ СЕРВИСА -------

Route::prefix('/auction')->group(function () {
    // Получить активные раунды
    Route::get('/active-rounds/list/{auction_type}', [RoundAuctionController::class, 'activeRounds']);
    // Основная выборка лотов для аукциона
    Route::post('/active-round-auction/lots/{round_id}', [LotController::class, 'getActiveRoundLots']);
    // Получить количество лотов активного раунда
    Route::post('/active-round-auction/lot-counts/{round_id}', [LotController::class, 'getActiveRoundLotsCount']);
    // Получить марки, модели авто, города ...
    Route::get('/search-form/get-form-param/{auction_type}', [AuctionController::class, 'getSearchFormParams']);
    // Сделать ставку
    Route::post('/bid/set-bid', [BidController::class, 'setBid']);
    // Купить сейчас
    Route::post('/bid/set-buy-now', [BidController::class, 'setBuyNow']);
    // Получить ставки лота
    Route::get('/lot/bids-list/{lot_id}', [BidController::class, 'getLotBids']);

    // TODO выборка лотов аукциона для тестирования (не забыть убрать)
    Route::get('/active-round-auction/list/{round_id}', [LotController::class, 'getActiveRoundLotsTest']);
});


/////////////////////////////////////////////////
// ------- АДМИНИСТРАТИВНАЯ ЧАСТЬ СЕРВИСА -------
Route::prefix('/admin')->group(function () {
    // Получить все раунды
    Route::get('/round-auction/all-rounds/{auction_type}/{limit}', [RoundAuctionController::class, 'index']);
    // Создать новый раунд аукциона
    Route::post('/round-auction/create', [RoundAuctionController::class, 'create']);
    // Удалить раунд аукциона
    Route::get('/round-auction/delete/{round_id}', [RoundAuctionController::class, 'deleteRound']);
    // Получить 1 раунд
    Route::get('/round-auction/item/{round_id}', [RoundAuctionController::class, 'getRound']);
    // Добавить лоты на аукцион
    Route::post('/lot-auction/add-lots', [LotController::class, 'addLots']);
});

Route::prefix('/users')->group(function () {
    Route::post('/register', [UserController::class, 'register']);
    Route::put('/update/{user_id}', [UserController::class, 'update']);
    Route::get('/list', [UserController::class, 'getUsers']);
    Route::get('/user-by-id/{user_id}', [UserController::class, 'getUserById']);
    Route::delete('/delete/{id}', [UserController::class, 'delete']);
    Route::post('/login', [SecurityController::class, 'login']);
    Route::get('/logout', [SecurityController::class, 'logout']);
});

Route::prefix('/profiles')->group(function () {
    Route::post('/create', [ProfileController::class, 'create']);
    Route::put('/update/{id}', [ProfileController::class, 'update']);
    Route::get('/profile/{id}', [ProfileController::class, 'getProfileById']);
    Route::get('/list-all', [ProfileController::class, 'getProfiles']);
    Route::get('/list-by/{fname}/{fvalue}', [ProfileController::class, 'getProfilesBy']);
    Route::delete('/delete/{id}', [ProfileController::class, 'delete']);
    Route::get('/set-active/{profile_id}', [ProfileController::class, 'setActiveProfile']);
});

Route::get('/test-jwt-auth-user', function (Request $request) {
    return $request->user();
})->middleware("jwt_auth_user");

///////////////////////////////////////////
// ------- ВСПОМОГАТЕЛЬНЫЕ СЕРВИСЫ -------
Route::get('/migrate-make-start', [MakeMigrateWorkerController::class, 'run']);
