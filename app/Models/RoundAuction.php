<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoundAuction extends Model
{

    public $table = 'rounds_auction';

    // Получить все раунды
    public function getRounds($where = [], $limit, $orderBy = 'desc') {
        if(!empty($where)) $collection = $this->where($where);
        else               $collection = $this->select('*');
        $list = $collection->orderBy('start_date', $orderBy)->limit($limit)->get();
        $result = $this->getLotsCount($list);
        return $result;
    }

    // Получить активные раунды
    public function getActiveRounds($auctionType = 1) {

        $table = $this->table;
        $today = date("Y-m-d H:i");
        $where = [
            ["{$table}.auction_type", '=', $auctionType],
            ['end_date', '>', $today],
        ];
        $list = $this->select("{$table}.*")
                        ->where($where)
                        ->whereExists(function($query) use ($table) {
                            $query->from('lots')->whereRaw("lots.round_id = {$table}.id");
                        })->orderBy('start_date', 'asc')->get()->toArray();

        $lotStatus = 1;
        $result = $this->getLotsCount($list, $lotStatus);
        return $result;
    }

    // Получить 1 раунд
    public function getRound($where = []) {
        $result = $this->where($where)->first();
        return $result;
    }

    // Удалить раунд
    public function deleteRound($roundId) {
        $result = $this->where('id', '=', $roundId)->delete();
        return $result;
    }

    public function getLotsCount($result, $lotStatus = 1) {
        foreach ($result as $key => $item) {
            $roundId = $item['id'];
            $where = [['round_id', $roundId],
                      ['status', $lotStatus]];
            $lotsCount = DB::table('lots')->where($where)->count();
            $result[$key]['lots_count'] = $lotsCount;
        }
        return $result;
    }

    //protected $casts = ['is_published' => 'boolean', 'published_at' => 'datetime:Y-m-d',]; // Преобразование атрибутов
    //protected $hidden = ['password'];  //  Видимость
    // protected $primaryKey = 'round_id';
    // protected $visible = ['first_name', 'last_name'];
    // protected $fillable = ['name'];
    // public function created(){ file_put_contents('file.txt', 'Ваш превосходный текст'); }
}
