<?php

namespace App\Services\Interfaces;

interface SenderServiceInterface
{
    public function sendMail($message, $email);

    public function sendSms($message, $phone);
}
