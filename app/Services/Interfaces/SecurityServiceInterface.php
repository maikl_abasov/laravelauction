<?php


namespace App\Services\Interfaces;


interface SecurityServiceInterface
{
    public function verifyToken($token);
    public function createToken(array $data);
}
