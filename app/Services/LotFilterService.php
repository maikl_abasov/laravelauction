<?php

namespace App\Services;

class LotFilterService
{
    private $request;
    private $builder;

    public function __construct($request, $builder = []) {
        $this->request = $request;
        $this->builder = $builder;
    }

    protected function sortList() {
        return [
            'start_price' => 'cars.start_price',
            'year'        => 'cars.year',
        ];
    }

    protected function filtersList() {
        return [
            'city_id'    => ['geo_cities.city_id' , '='],
            'lot_id'     => ['lots.id', '='],
            'mark_id'    => ['avc_marks.id' , '='],
            'model_id'   => ['avc_models.id', '='],

            'nds'        => ['cars.nds'     , '='],
            'vin'        => ['cars.vin'     , 'like'],
            'price_from' => ['cars.start_price' , '>='],
            'price_to'   => ['cars.start_price'   , '<='],
        ];
    }

    public function apply($condition = []) {

        $filters = $this->filtersList();
        $request = $this->request;
        $where   = [];
        foreach ($filters as $filterName => $filter)  {
            if(!empty($request[$filterName])) {
                $value = $request[$filterName];
                $value = ($filter[1] == 'like') ? "%$value%" : $value;
                $filter[] = $value;
                $where[]  = $filter;
            }
        }

        if(!empty($condition['round_id']))
            $where[] = ["lots.round_id", '=', $condition['round_id']];

        if(!empty($condition['lot_status']))
            $where[] = ['lots.status', '=', $condition['lot_status']];

        if(!empty($condition['auction_type']))
            $where[] = ['rounds_auction.auction_type', '=', $condition['auction_type']];

        return $where;
    }

    public function sort($builder = []) {

        $sortByList = $this->sortList();

        $builder = (!empty($builder)) ? $builder : $this->builder;
        $request = $this->request;

        if(empty($request['order_by'])) { // если пустой то сортируем по цене
            $builder->orderBy('cars.start_price', 'asc');
            return $builder;
        }

        foreach ($sortByList as $name => $sortName) {
            if(isset($request['order_by'][$name])) {
                $value = $request['order_by'][$name];
                $builder->orderBy($sortName, $value);
                break;
            }
        }

        return $builder;
    }

}
