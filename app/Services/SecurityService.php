<?php


namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use DomainException;
use InvalidArgumentException;
use UnexpectedValueException;

use App\Services\Interfaces\SecurityServiceInterface;


class SecurityService implements SecurityServiceInterface
{
    private $codeAlgo = "HS256";
    private $secretKey = "5r6tu47$$%gfgsksl879jhgnv&TYugfhtrbghh52343HJfd&54##434";

    public function createToken($data) {
        $payload = [
            'iss' => 'example.org',
            'aud' => 'example.com',
            'iat' => 1356999524,
            'nbf' => 1357000000,
            'data' => $data,
        ];
        $key = $this->secretKey;
        $token = JWT::encode($payload, $key, $this->codeAlgo);
        return $token;
    }

    public function createRefreshToken() {
        $payload = [
            'iss' => 'example.org',
            'aud' => 'example.com',
        ];
        $key = $this->secretKey;
        $token = JWT::encode($payload, $key, $this->codeAlgo);
        return $token;
    }

    public function verifyToken($token) {

        $key = $this->secretKey;
        try {
            $decodedData = JWT::decode($token, new Key($key, $this->codeAlgo));
        } catch (InvalidArgumentException $e) {
            return $this->errorResponse($e, 'InvalidArgumentException');
        } catch (DomainException $e) {
            return $this->errorResponse($e, 'DomainException');
        } catch (SignatureInvalidException $e) {
            return $this->errorResponse($e, 'SignatureInvalidException');
        } catch (BeforeValidException $e) {
            return $this->errorResponse($e, 'BeforeValidException');
        } catch (ExpiredException $e) {
            return $this->errorResponse($e, 'ExpiredException');
        } catch (UnexpectedValueException $e) {
            return $this->errorResponse($e, 'UnexpectedValueException');
        }

        return [ 'data' => $decodedData, 'error' => false];
    }

    public function errorResponse($err, $type) {
        $error = [
            'error_type' => $type,
            'error'      => true,
            'code'       => $err->getCode(),
            'message'    > $err->getMessage()
        ];
        return $error;
    }

}
