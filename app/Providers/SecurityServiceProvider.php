<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Interfaces\SecurityServiceInterface;
use App\Services\SecurityService;

class SecurityServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            SecurityServiceInterface::class,
            SecurityService::class
        );
    }

    public function boot()
    {
        //
    }
}
