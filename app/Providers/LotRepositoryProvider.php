<?php

namespace App\Providers;

use App\Repositories\Interfaces\LotRepositoryInterface;
use App\Repositories\LotRepository;
use Illuminate\Support\ServiceProvider;

class LotRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LotRepositoryInterface::class,
            LotRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
