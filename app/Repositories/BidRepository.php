<?php


namespace App\Repositories;


use App\Models\Bid;
use App\Repositories\Interfaces\LotRepositoryInterface;
use Illuminate\Support\Facades\DB;

class BidRepository
{
    private Bid $model;
    private $lotRepo;
    public function __construct(Bid $model, LotRepositoryInterface $lot) {
        $this->model = $model;
        $this->lotRepo = $lot;
    }

    public function setBid($data) {

        if(!$this->checkNewBid($data))
            return false;

        $bidStatus = 1;
        $save = $this->model->insert([
            'price'      => $data['bid_price'],
            'lot_id'     => $data['lot_id'],
            'profile_id' => $data['profile_id'],
            'bid_status' => $bidStatus,
        ]);
        return $save;
    }

    public function setBuyNow($data) {
        $bidStatus = 2;
        $lotId = $data['lot_id'];
        $save = $this->model->insert([
            'lot_id'     => $lotId,
            'price'      => $data['bid_price'],
            'profile_id' => $data['profile_id'],
            'bid_status' => $bidStatus,
        ]);

        if($save) {
           $lotUpdate = $this->lotRepo->updateLot($lotId, ['status' => 4]);
        }

        return $save;
    }

    public function checkNewBid($data) {

        $newPrice  = $data['bid_price'];
        $lotId     = $data['lot_id'];
        $step      = $data['step_price'];
        $bid = $this->model->where(['lot_id' => $lotId])
                           ->orderBy('created_at', 'desc')->first();
        if(empty($bid)) {
            $car = DB::table('lots')->join('cars', 'lots.car_id', 'cars.id')
                                         ->where('lots.id', $lotId)->select('cars.start_price')->first();
            $startPrice = $car->start_price + $step;
            if($newPrice < $startPrice)
                return false;
            return true;
        }

        $lastPrice = $bid->price + $step;
        if($newPrice < $lastPrice)
            return false;
        return true;
    }

    public function getLotBids($lotId) {
        $result = $this->model->where('lot_id', $lotId)->orderBy('created_at', 'desc')->get();
        return $result;
    }

    public function getBid($where) {
        $result = $this->model->where($where)->first();
        return $result;
    }

    public function getBidList($where) {
        $result = $this->model->where($where)->get()->toArray();
        return $result;
    }

}
