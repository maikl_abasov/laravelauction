<?php

namespace App\Repositories;

use App\Models\Profile;

class ProfileRepository
{

    private $model;

    public function __construct(Profile $model) {
        $this->model = $model;
    }

    public function create($data) {
        return $this->model->insert($data);
    }

    public function update($id, $data) {
        return $this->model->where('id', $id)->update($data);
    }

    public function delete($id) {
        $result = $this->model->where('id', $id)->delete();
        return $result;
    }

    public function getProfiles() {
        $result = $this->model->orderBy('created_at', 'desc')->get();
        return $result;
    }

    public function getProfileById($id) {
        $result = $this->model->find($id);
        return $result;
    }

    // Устанавливаем активный профиль
    public function setActiveProfile($id) {
        $profile = $this->getProfileById($id);
        $this->model->where('user_id', $profile->user_id)->update(['active' => 0]);
        $update = $this->model->where('id', $id)->update(['active' => 1]);
        return $update;
    }

    public function getProfilesBy(array $where) {
        $result = $this->model->where($where)
                              ->orderBy('created_at', 'desc')->get();
        return $result;
    }

}
