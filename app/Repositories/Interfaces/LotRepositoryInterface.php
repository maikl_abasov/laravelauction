<?php


namespace App\Repositories\Interfaces;

interface LotRepositoryInterface
{
    public function addLots(array $carVins, array $post): array;
}
