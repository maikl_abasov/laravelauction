<?php


namespace App\Repositories;


use App\Models\User;

class UserRepository
{
    private $model;

    public function __construct(User $model) {
        $this->model = $model;
    }

    public function register(array $data) {
        $save = $this->model->insert($data);
        return $save;
    }

    public function update($userId,array $data) {
        $save = $this->model->where('id', $userId)->update($data);
        return $save;
    }

    public function delete($id) {
        $result = $this->model->where('id', $id)->delete();
        return $result;
    }

    public function getUsers() {
        $result = $this->model->orderBy('created_at', 'desc')->get();
        return $result;
    }

    public function getUserById($userId) {
        $result = $this->model->find($userId);
        return $result;
    }

}
