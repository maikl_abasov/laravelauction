<?php

namespace App\Repositories;

use App\Repositories\Interfaces\LotRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Services\LotFilterService;
use App\Models\Lot;

class LotRepository implements LotRepositoryInterface
{
    private Lot $model;

    public function __construct(Lot $model) {
        $this->model = $model;
    }

    // Обновление лота
    public function updateLot($lotId, array $data) {
        $update = $this->model->where('id', $lotId)->update($data);
        return $update;
    }

    // Создание новых лотов
    public function addLots(array $carVins, array $post): array {

         $data = [
            'round_id'     => $post['round_id'],
            'auction_type' => $post['auction_type'],
            'category_id'  => $post['category_id'],
            'profile_id'   => $post['profile_id'],
            'status'       => $post['status'],
            'car_id'       => null,
         ];

         $resp = [];
         foreach ($carVins as $key => $vin) {
             $car = DB::table('cars')->select('id')->where('vin', $vin)->first();
             $data['car_id'] = $car->id;
             $resp[$vin] = $this->model->insert($data);
         }

         return $resp;
    }

    // Получить количество лотов активного раунда
    public function getActiveRoundLotsCount($condition, $filtersData = []) {
           $builder = $this->getJoinBuilder();
           $filterService = new LotFilterService($filtersData, $builder);
           $where = $filterService->apply($condition); // Формируем фильтры
           return $builder->where($where)->count(); // Получаем количество лотов
    }

    // Получить активный раунд (основная выборка аукциона)
    public function getActiveRoundLots($condition, $filtersData = []) {

            $builder = $this->getJoinBuilder();
            $filterService = new LotFilterService($filtersData, $builder);
            $where     = $filterService->apply($condition); // Формируем фильтры
            $builder   = $builder->where($where);
            $lotCounts = $builder->count(); // Получаем количество лотов

            $page   = $filtersData['page'];
            $limit  = $filtersData['limit'];
            $offset = ($page-1) * $limit;
            $selected = $this->selectedFieldNames();

            // ---- Получаем лоты -----
            $collection = $filterService->sort($builder);
            $collection = $collection->select($selected)
                                     ->offset($offset)
                                     ->limit($limit)
                                     ->get()->toArray();

            $list = $this->getCarPhotosAndBids($collection); // Получаем фото и ставки

            return [
               'list'   => $list,
               'counts' => $lotCounts
            ];
    }

    public function selectedFieldNames() {
        return  [
            "lots.car_id",
            "lots.round_id",
            "lots.id         AS lot_id",
            "lots.status     AS lot_status",
            "lots.profile_id AS lot_profile_id",

            "avc_marks.name  AS mark_name",
            "avc_marks.id    AS mark_id",
            "avc_models.name AS model_name",
            "avc_models.id   AS model_id",

            "avc_generations.name   AS generation_name",
            "avc_generations.id     AS generation_id",

            "avc_modifications.name AS modification_name",
            "avc_modifications.id   AS modification_id",
            "avc_modifications.body_type",
            "avc_modifications.fuel_type",
            "avc_modifications.drive_type",
            "avc_modifications.transmission",
            "avc_modifications.power",
            "avc_modifications.engine_size",

            "geo_cities.city AS city_name",
            "geo_cities.city_id",

            "rounds_auction.start_date",
            "rounds_auction.end_date",
            "rounds_auction.auction_type",

            // "car.start_price", "car.vin",
            "cars.*",
        ];
    }

    public function getJoinBuilder() {
        return $this->model
            ->join('rounds_auction', 'lots.round_id', '=', 'rounds_auction.id')
            ->join('cars', 'lots.car_id', '=', 'cars.id')
            ->leftJoin('geo_cities', 'cars.city', '=', 'geo_cities.city_id')
            ->leftJoin('avc_marks' , 'cars.mark_id', '=', 'avc_marks.id')
            ->leftJoin('avc_models', 'cars.model_id',     '=', 'avc_models.id')
            ->leftJoin('avc_generations',   'cars.generation_id', '=', 'avc_generations.id')
            ->leftJoin('avc_modifications', 'cars.modification_id', '=', 'avc_modifications.id');
    }

    public function getCarPhotosAndBids(array $list) {
        foreach ($list as $key => $item) {
            $carId = $item['car_id'];
            $lotId = $item['lot_id'];
            $auсtionType = $item['auction_type'];

            $photos = DB::table('car_photos')->where('car_id', $carId)->get()->toArray();
            $list[$key]['photos'] = $photos;

            $bids = DB::table('bids')->where('lot_id', $lotId)->orderBy('created_at', 'desc')->first();
            $list[$key]['last_bid'] = $bids;
        }
        return $list;
    }

}



/**************
// Вариант 2
->leftJoin('avc_marks  AS mark' , 'cars.mark_id', '=', 'mark.id')
->leftJoin('avc_models AS model', 'mark.id',     '=', 'model.mark')
->leftJoin('avc_generations   AS generation',   'model.id', '=', 'generation.model')
->leftJoin('avc_modifications AS modification', 'generation.id', '=', 'modification.generation')
 * ***********/

/**************
// Вариант 3
->leftJoin('avc_modifications AS modification', 'cars.modification_id', '=', 'modification.id')
->leftJoin('avc_generations   AS generation',   'modification.generation', '=', 'generation.id')
->leftJoin('avc_models AS model', 'generation.model', '=', 'model.id')
->leftJoin('avc_marks  AS mark' , 'model.mark', '=', 'mark.id')
 **************/
