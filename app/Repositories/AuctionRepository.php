<?php


namespace App\Repositories;

use App\Models\RoundAuction;
use Illuminate\Support\Facades\DB;

class AuctionRepository
{
    public function getSearchFormParams($auctionType){

        //$round = new RoundAuction();
        //$activeRounds = $round->getActiveRounds($auctionType);
        //foreach ($activeRounds)

        $today = date("Y-m-d H:i");
        $where = [
            ['round.auction_type', '=', $auctionType],
            ['round.end_date', '>', $today],
        ];

        $selectedFields = [

            "mark.name  AS mark_name",
            "mark.id    AS mark_id",
            "model.name AS model_name",
            "model.id   AS model_id",

            "generation.name   AS generation_name",
            "generation.id     AS generation_id",

            "modification.name AS modification_name",
            "modification.id   AS modification_id",
            "modification.body_type",
            "modification.fuel_type",
            "modification.drive_type",
            "modification.transmission",
            "modification.power",
            "modification.engine_size",

            "city.city AS city_name",
            "city.city_id",

        ];

        $result = DB::table('rounds_auction AS round')
                    ->join('lots AS lot', 'round.id', '=', 'lot.round_id')
                    ->join('cars AS car', 'lot.car_id', '=', 'car.id')
                    ->leftJoin('geo_cities AS city', 'car.city', '=', 'city.city_id')

                    ->leftJoin('avc_marks  AS mark' , 'car.mark_id', '=', 'mark.id')
                    ->leftJoin('avc_models AS model', 'car.model_id',     '=', 'model.id')
                    ->leftJoin('avc_generations   AS generation',   'car.generation_id', '=', 'generation.id')
                    ->leftJoin('avc_modifications AS modification', 'car.modification_id', '=', 'modification.id')

                    ->where($where)
                    ->select($selectedFields)
                    ->orderBy('start_date', 'asc')
                    ->get()->toArray();

        $result = $this->formParamsFormat($result);

        return $result;
    }

    public function formParamsFormat($data) {

        $marks = $models = $cities = [];
        foreach ($data as $item) {
            $item = (array)$item;

            $markId  = $item['mark_id'];
            $modelId = $item['model_id'];
            $cityId  = $item['city_id'];

            $marks[$markId]   = $item;
            $models[$modelId] = $item;
            $cities[$cityId]  = $item;
        }

        return  [
            'marks'  => $marks,
            'models' => $models,
            'cities' => $cities
        ];
    }
}
