<?php

namespace App\Http\Middleware;

use App\Services\Interfaces\SecurityServiceInterface;
use App\Services\SecurityService;
use Closure;

class JwtAuthUser
{

    private $securityService;

    public function __construct(SecurityServiceInterface $service) {
        $this->securityService = $service;
    }

    public function handle($request, Closure $next)
    {
        $data = ['data' => [], 'error' => false];
        $token = $request->header("X-JWT-USER-TOKEN");

        if ($token) {
            $data = $this->securityService->verifyToken($token);
            if(!empty($data['error'])) {
                return response()->json([
                    'jwt_auth_error' => $data
                ], '403');
            }
        }

        $request->JWT_TOKEN_INFO = $data['data'];
        return $next($request);
    }
}
