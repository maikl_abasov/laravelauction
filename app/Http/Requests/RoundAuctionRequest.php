<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoundAuctionRequest extends FormRequest
{

    public function authorize() {
        return true;
    }

    public function rules(){
        return [
            'auction_type' => 'required',
            'start_date'   => 'required',
            'end_date'     => 'required',
            'date'         => 'nullable',
            'time'         => 'nullable',
            'profile_id'   => 'nullable',
        ];
    }

    public function messages(){
        return [
            'start_date.required'   => 'Начало аукциона не заполнено',
            'end_date.required'     => 'Окончание аукциона не заполнено',
            'auction_type.required' => 'Тип торгов не выбран',
        ];
    }
}
