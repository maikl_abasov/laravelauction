<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LotRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'auction_type' => 'required',
            'car_vins'     => 'required',
            'round_id'     => 'required',
            'profile_id'   => 'nullable',
            'category_id'  => 'nullable',
            'status'       => 'nullable',
        ];
    }

    public function messages(){
        return [
            'car_vins.required'     => 'Нет данных авто (vin)',
            'round_id.required'     => 'Не выбран раунд аукциона',
            'auction_type.required' => 'Тип торгов не выбран',
        ];
    }
}
