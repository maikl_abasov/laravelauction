<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize() {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|min:3|max:60',
            'email'    => 'required|unique:users,email|email|max:40',
            'password' => 'required|max:100|confirmed',

            'phone'    => 'nullable',
            'role'     => 'nullable',
            'active'   => 'nullable',
        ];
    }

    public function messages(){
        return [
            'username.required'  => 'Не задано имя пользователя.',
            'email.email'        => 'Неправильный формат почты',
            'email.unique'       => 'Пользователь с такой почтой уже существует',
            'password.required'  => 'Не задан пароль',
            'password.confirmed' => 'Пароли не совпадают',
        ];
    }

}
