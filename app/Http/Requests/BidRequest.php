<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BidRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'bid_price'  => 'required',
            'lot_id'     => 'required',
            'profile_id' => 'nullable',
            'round_id'   => 'nullable',
            'step_price' => 'nullable',
        ];
    }

    public function messages(){
        return [
            'bid_price'  => 'Не задана сумма ставки',
            'lot_id'     => 'Не выбран лот',
        ];
    }

}
