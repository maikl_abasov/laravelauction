<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends ApiBaseController
{

    private $repo;

    public function __construct(UserRepository $repo) {
        $this->repo = $repo;
    }

    public function passwordHash($data) {
        return password_hash($data, PASSWORD_BCRYPT);
    }

    public function register(UserRequest $request) {
        $data   = $request->validated(); // Валидируем данные
        $data['password'] = $this->passwordHash($data['password']);
        $result = $this->repo->register($data);
        return $this->responseJson(['save' => $result]);
    }

    public function update($user_id, Request $request) {
        $data   = $this->updateDataValid($request); // Валидируем данные
        $result = $this->repo->update($user_id, $data);
        return $this->responseJson(['save' => $result]);
    }

    public function delete($id) {
        $result = $this->repo->delete($id);
        return $this->responseJson(['delete' => $result]);
    }

    public function getUsers() {
        $list = $this->repo->getUsers();
        return $this->responseJson($list);
    }

    public function getUserById($user_id) {
        $user = $this->repo->getUserById($user_id);
        return $this->responseJson($user);
    }

    public function updateDataValid($request) {

        $rules = [
            'username' => 'required|min:3|max:60',
            'phone'    => 'nullable',
            'role'     => 'nullable',
            'active'   => 'nullable',
        ];

        $messages = [
            'username.required'  => 'Не задано имя пользователя.',
        ];

        $data  = $request->validate($rules, $messages);

        return $data;
    }

}
