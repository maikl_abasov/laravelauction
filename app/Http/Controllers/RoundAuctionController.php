<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoundAuction;
use App\Http\Requests\RoundAuctionRequest;

class RoundAuctionController extends ApiBaseController
{
    private $model;
    public function __construct(RoundAuction $roundModel) {
        $this->model = $roundModel;
    }

    public function create(RoundAuctionRequest $request) {
        $data   = $request->validated();
        $status = $this->model->insert($data);
        return $this->responseJson(['save' => $status]);
    }

    public function index($aution_type, $limit) {
        $limit = ($limit) ? $limit : null;
        $where = ($aution_type) ? array(['auction_type', '=', $aution_type]) : [];
        $result = $this->model->getRounds($where, $limit);
        return $this->responseJson($result);
    }

    public function activeRounds($aution_type) {
        $result = $this->model->getActiveRounds($aution_type);
        return $this->responseJson($result);
    }

    public function getRound($roundId) {
        $data = $this->model->getRound(array(['id', '=', $roundId]));
        return $this->responseJson($data);
    }

    public function deleteRound($roundId) {
        $result = $this->model->deleteRound($roundId);
        return $this->responseJson(['delete' => $result]);
    }
}
