<?php

namespace App\Http\Controllers;

use App\Http\Requests\BidRequest;
use App\Repositories\BidRepository;

class BidController extends ApiBaseController
{
    protected $repo;
    public function __construct(BidRepository $repo) {
        $this->repo = $repo;
    }

    public function setBid(BidRequest $request) {
        $data = $request->validated();
        $save = $this->repo->setBid($data);
        return $this->responseJson(['save' => $save]);
    }

    public function setBuyNow(BidRequest $request) {
        $data = $request->validated();
        $save = $this->repo->setBuyNow($data);
        return $this->responseJson(['save' => $save]);
    }

    public function getLotBids($lot_id) {
        $result = $this->repo->getLotBids($lot_id);
        return $this->responseJson($result);
    }
}
