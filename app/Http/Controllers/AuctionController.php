<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuctionRepository;

class AuctionController extends ApiBaseController
{
    private $repo;

    public function __construct(AuctionRepository $repo) {
        $this->repo = $repo;
    }

    public function getSearchFormParams($auction_type) {
        $result = $this->repo->getSearchFormParams($auction_type);
        return $this->responseJson($result);
    }
}
