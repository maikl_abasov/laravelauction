<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LotRequest;
use App\Repositories\Interfaces\LotRepositoryInterface;
// use App\Repositories\LotRepository;

class LotController extends ApiBaseController
{
    private $repo;

    public function __construct(LotRepositoryInterface $repo) {
        $this->repo = $repo;
    }

    public function addLots(LotRequest $request) {
        $data = $request->validated();
        $carVins = explode("\n", $data['car_vins']);
        $resp = $this->repo->addLots($carVins, $data);
        $lotsCount = count($resp);
        $result = [
            'save'       => $lotsCount,
            'save_count' => $lotsCount,
            'vins_count' => count($carVins)
        ];
        return $this->responseJson($result);
    }

    // Получить количество лотов активного раунда
    public function getActiveRoundLotsCount($round_id, Request $request) {

        $filtersData = $request->all();
        $lotStatus   = 1; // Лот активный, на торгах
        $auctionType = 1; // Тип торгов, открытые торги

        $condition = [
            'round_id'     => $round_id,
            'lot_status'   => $lotStatus,
            'auction_type' => $auctionType
        ];

        $result = $this->repo->getActiveRoundLotsCount($condition, $filtersData);
        return $this->responseJson($result);
    }

    // Основная выборка лотов для аукциона
    public function getActiveRoundLots($round_id, Request $request) {

        $filtersData = $request->all();
        $lotStatus   = 1; // Лот активный, на торгах
        $auctionType = 1; // Тип торгов, открытые торги

        $condition = [
            'round_id'     => $round_id,
            'lot_status'   => $lotStatus,
            'auction_type' => $auctionType
        ];

        $result = $this->repo->getActiveRoundLots($condition, $filtersData);
        return $this->responseJson($result);
    }

    //TODO для тестирования (не забыть убрать)
    public function getActiveRoundLotsTest($round_id) {

        $lotStatus   = 1; // Лот активный, на торгах
        $auctionType = 1; // Тип торгов, открытые торги

        $condition = [
           'round_id'     => $round_id,
           'lot_status'   => $lotStatus,
           'auction_type' => $auctionType
        ];

        $filtersData = [
            'city_id'    => '2287',
            'lot_id'     => '',
            'mark_id'    => 0,
            'model_id'   => 0,
            'nds'        => 0,
            'vin'        => '',
            'price_from' => '',
            'price_to'   => '',
            'page'  => 1,
            'limit' => 5,

            'order_by' => ['start_price' => 'desc'],
        ];

        $result = $this->repo->getActiveRoundLots($condition, $filtersData);

        lg($result);

        // return $this->responseJson($result);
    }

}
