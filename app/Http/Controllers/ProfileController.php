<?php

namespace App\Http\Controllers;

use App\Repositories\ProfileRepository;
use Illuminate\Http\Request;

class ProfileController extends ApiBaseController
{
    private $repo;

    public function __construct(ProfileRepository $repo) {
        $this->repo = $repo;
    }

    public function create(Request $request) {
        $data   = $this->insertValidate($request); // Валидируем данные
        $result = $this->repo->create($data);
        return $this->responseJson(['save' => $result]);
    }

    public function update($id, Request $request) {
        $data   = $this->updateValidate($request); // Валидируем данные
        $result = $this->repo->update($id, $data);
        return $this->responseJson(['save' => $result]);
    }

    public function delete($id) {
        $result = $this->repo->delete($id);
        return $this->responseJson(['delete' => $result]);
    }

    public function getProfiles() {
        $list = $this->repo->getProfiles();
        return $this->responseJson($list);
    }

    public function getProfilesBy($fname, $fvalue) {
        $data = [$fname => $fvalue];
        $result = $this->repo->getProfilesBy($data);
        return $result;
    }

    public function getProfileById($id) {
        $profile = $this->repo->getProfileById($id);
        return $this->responseJson($profile);
    }

    public function setActiveProfile($id) {
        $profile = $this->repo->setActiveProfile($id);
        return $this->responseJson($profile);
    }

    public function insertValidate($request) {

        $rules = [
            'name'         => 'required|min:3|max:120',
            'user_id'      => 'required',
            'email'        => 'nullable',
            'phone'        => 'nullable',
            'profile_type' => 'nullable',
            'active'       => 'nullable',
        ];

        $messages = [
            'name.required'  => 'Не задано имя профиля.',
            'user_id.required'  => 'Не выбран пользователь.',
        ];

        $data  = $request->validate($rules, $messages);

        return $data;
    }

    public function updateValidate($request) {

        $rules = [
            'name'         => 'required|min:3|max:120',
            // 'user_id'      => 'required',
            'email'        => 'nullable',
            'phone'        => 'nullable',
            'profile_type' => 'nullable',
            'active'       => 'nullable',
        ];

        $messages = [
            'name.required'  => 'Не задано имя профиля.',
            'user_id.required'  => 'Не выбран пользователь.',
        ];

        $data  = $request->validate($rules, $messages);

        return $data;
    }
}
