<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiBaseController extends Controller
{
    protected function responseJson($data, $error = [], $code = 200, $headers = []) {
        $responseData = [
           'result' => $data,
           'error'  => $error,
        ];
        return response()->json($responseData, $code, $headers, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

}
