<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Services\Interfaces\SecurityServiceInterface;

class SecurityController extends ApiBaseController
{
    private $service;

    public function __construct(SecurityServiceInterface $service) {
       $this->service = $service;
    }

    public function login(Request $request) {

        $email    = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email', $email)->first();

        if (!password_verify($password, $user->password)) {
            $errMessage = 'Не удалось авторизироваться, неправильный логин или пароль';
            return $this->responseJson([], ['message' => $errMessage]);
        }

        unset($user->password);

        $profile = Profile::where([
            ['user_id', $user->id],
            ['active', true],
        ])->first();

        $user['profile'] = [];
        $user['profile_id'] = 0;

        if(!empty($profile)) {
            $user['profile'] = $profile;
            $user['profile_id'] = $profile->id;
        }

        $token =  $this->service->createToken(['user' => $user]);
        $refreshToken = $this->service->createRefreshToken();

        return $this->responseJson([
            'user'    => $user,
            'token'   => $token,
            'refresh_token' => $refreshToken,
            'message' => 'Успешная авторизация!'
        ]);
    }

    public function logout(Request $request) {
        //$result = ['success' => true, 'result' => 'Данные успешно сохранены'];
        //return $this->json($result);
    }
}
